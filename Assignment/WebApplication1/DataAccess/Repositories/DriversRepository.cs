﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models.Domain;


namespace WebApplication1.DataAccess.Repositories
{
    public class DriversRepository : IDriversRepository
    {
        private readonly ApplicationDbContext _context;
        public DriversRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void AddDriver(Driver d)
        {
            _context.Drivers.Add(d);
            _context.SaveChanges();
        }

        public void DeleteDriver(Guid id)
        {
            _context.Drivers.Remove(GetDriver(id));
            _context.SaveChanges();
        }

        public Driver GetDriver(Guid id)
        {
            return _context.Drivers.SingleOrDefault(x => x.DriverId == id);
        }

        public IQueryable<Driver> GetDriver()
        {
            return _context.Drivers;
        }

        public void UpdateDriver(Driver d)
        {
            var originalDriver = GetDriver(d.DriverId);
            //originalDriver.Url = d.Url;
            _context.SaveChanges();
        }
    }
}
