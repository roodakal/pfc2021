﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models.Domain;

namespace WebApplication1.DataAccess.Repositories
{
    public class RidesRepository : IRidesRepository
    {
        private readonly ApplicationDbContext _context;

        public RidesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddRide(Ride r)
        {
            _context.Rides.Add(r);
            _context.SaveChanges();
        }

        public void DeleteRide(Guid id)
        {
            _context.Rides.Remove(GetRide(id));
            _context.SaveChanges();
        }

        public Ride GetRide(Guid id)
        {
            return _context.Rides.SingleOrDefault(x => x.RideId == id);
        }

        public IQueryable<Ride> GetRides()
        {
            return _context.Rides;
        }

        public void UpdateRide(Ride r)
        {
            var originalRide = GetRide(r.RideId);
            //originalRide.Url = r.Url;
            _context.SaveChanges();
        }
    }
}
