﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize(Roles ="ADMIN")]
    public class AdminController : Controller
    {
        private readonly ICacheRepository _cacheRepo;
        private readonly IRidesRepository _ridesRepo;
        public AdminController (ICacheRepository cacheRepo)
        {
            _cacheRepo = cacheRepo;
        }
        
        [HttpPost]
        public IActionResult Check(Category c)
        {
            _cacheRepo.UpsertCategory(c);
            return View();
        }

        public IActionResult Index()
        {
            var list = _ridesRepo.GetRides();
            return View();
        }
        
    }
}
