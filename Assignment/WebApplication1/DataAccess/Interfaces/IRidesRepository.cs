﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models.Domain;

namespace WebApplication1.DataAccess.Interfaces
{
    interface IRidesRepository
    {
        void AddRide(Ride r);
        Ride GetRide(Guid id);
        IQueryable<Ride> GetRides();

        void DeleteRide(Guid id);
        void UpdateRide(Ride r);
    }
}
