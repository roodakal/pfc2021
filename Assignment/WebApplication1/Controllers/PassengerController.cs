﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "PASSENGER")]
    public class PassengerController : Controller
    {
        private readonly ICacheRepository _cacheRepo;
        public PassengerController(ICacheRepository cacheRepo)
        {
            _cacheRepo = cacheRepo;
        }

        public IActionResult Create()
        {
            return View();
        }

        
    }
}
