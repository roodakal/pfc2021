using Google.Cloud.Diagnostics.AspNetCore;
using Google.Cloud.SecretManager.V1;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.DataAccess.Repositories;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGoogleExceptionLogging(options =>
            {
                options.ProjectId = "pfc2021-305709";
                options.ServiceName = "Pfc2021msd63a"; //random name where the logs will be categorized
                options.Version = "0.01";
            });
            
            
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    Configuration.GetConnectionString("DefaultConnection")));

            //these 3 lines basically specify to asp.net core what the dependency injector should follow in order to create the required instances
            services.AddScoped<IRidesRepository, RidesRepository>(); //how you can register classes/interfaces with the IOC container.
            services.AddScoped<IDriversRepository, DriversRepository>();
            services.AddScoped<ICacheRepository, CacheRepository>();
            services.AddScoped<IPubSubRepository, PubSubRepository>();
            services.AddScoped<ILogRepository, LogRepository>();

            //how you can register classes + interfaces which you have created with the IOC container
            
            
            //services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
            //    .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddDefaultUI()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();           
            
            services.AddControllersWithViews();
            services.AddRazorPages();

            
            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    //IConfigurationSection googleAuthNSection =
                    //    Configuration.GetSection("Authentication:Google");

                    //options.ClientId = googleAuthNsection["clientId"];
                    //options.ClientSecret = googleAuthNSection["ClientSecret"];

                    options.ClientId = GetGoogleClient("Authentication:Google:ClientId");
                    options.ClientSecret = GetGoogleClient("Authentication:Google:ClientSecret");
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();

            app.UseGoogleExceptionLogging();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }


        /*
         * to store in the secret manager
         * 
         * connectionstring
         * oAuth2.0 client id + secret key
         * 3rd party apikey using to send emails
         * 
         */


        public string GetGoogleClient(string key)
        {
            // Create the client.
            SecretManagerServiceClient client = SecretManagerServiceClient.Create();

            // Build the resource name.
            SecretVersionName secretVersionName = new SecretVersionName("pfc2021-305709", "ApiClientId", "1");

            // Call the API.
            AccessSecretVersionResponse result = client.AccessSecretVersion(secretVersionName);

            // Convert the payload to a string. Payloads are bytes by default.
            String payload = result.Payload.Data.ToStringUtf8();

            dynamic keys = JsonConvert.DeserializeObject(payload);

            JObject jObject = JObject.Parse(payload);
            JToken jKey = jObject[key];
            return jKey.ToString();            
        }
    }
}
