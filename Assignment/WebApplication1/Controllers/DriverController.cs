﻿using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models;
using WebApplication1.Models.Domain;

namespace WebApplication1.Controllers
{
    [Authorize(Roles ="DRIVER")]
    public class DriverController : Controller
    {
        private readonly IDriversRepository _driversRepo;
        private readonly IConfiguration _config;
        private readonly IPubSubRepository _pubsubRepo;
        public DriverController(IDriversRepository driversRepo, IConfiguration config, IPubSubRepository pubsubRepo)
        {
            _config = config;
            _driversRepo = driversRepo;
            _pubsubRepo = pubsubRepo;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Index()
        {
            
            return View();
        }
        
        [HttpPost]
        public IActionResult Create(IFormFile logo, Driver d)
        {
            try
            {
                string bucketName = _config.GetSection("AppSettings").GetSection("BucketName").Value;
                string uniqueFilename = Guid.NewGuid() + System.IO.Path.GetExtension(logo.FileName);

                //upload file to cloud storage
                var storage = StorageClient.Create();

                using (var myStream = logo.OpenReadStream())
                {
                    storage.UploadObject(bucketName, uniqueFilename, null, myStream);
                }

                //add a reference to the file url with the instance of the blog >>> b
                d.Photo = $"https://storage.googleapis.com/{bucketName}/{uniqueFilename}";

                //save everything in db
                _driversRepo.AddDriver(d);
               
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Driver wasn't created successfully";
                return View(d);
            }
        }
    }
}
