﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models.Domain;

namespace WebApplication1.DataAccess.Interfaces
{
    public interface IDriversRepository
    {
        void AddDriver(Driver d);
        Driver GetDriver(Guid id);
        IQueryable<Driver> GetDriver();

        void DeleteDriver(Guid id);
        void UpdateDriver(Driver d);
    }
}
