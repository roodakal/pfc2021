﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models.Domain
{
    public class Driver
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DriverId { get; set; }
        public string Type { get; set; }
        public int Capacity { get; set; }
        public string Condition { get; set; }
        public string Plate { get; set; }
        public bool Wifi { get; set; }
        public bool AC { get; set; }
        public bool Food { get; set; }
        public string Photo { get; set; }
    }
}
