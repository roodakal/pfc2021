﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DataAccess.Interfaces;

namespace WebApplication1.Controllers
{
    public class EmailController : Controller
    {
        private readonly IRidesRepository _ridesRepo;
        private readonly IConfiguration _config;
        private readonly IPubSubRepository _pubsubRepo;
        public EmailController(IConfiguration config, IPubSubRepository pubsubRepo)
        {
            _config = config;
            _pubsubRepo = pubsubRepo;
        }

        public IActionResult Pull()
        {
            string msgSerialized = _pubsubRepo.PullMessage(DataAccess.Repositories.Category.luxury);
            if (msgSerialized == string.Empty) return Content("No message read");
            dynamic myDeserializedData = JsonConvert.DeserializeObject(msgSerialized);
            string email = myDeserializedData.Email;
            string blogTitle = myDeserializedData.Blog.Title;
            string blogUrl = myDeserializedData.Blog.Url;

            //forming your email

            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", "2422ea1d39b0fcf8cdb5f43e4b8cd848-b6d086a8-94b4a531");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "sandboxc126092a388e421c88e1f5b82ed453e4.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "ianmark3107@gmail.com");
            request.AddParameter("to", email);            
            request.AddParameter("subject", "Receipt for inputting blog");
            request.AddParameter("text", "Blog " +blogTitle+ " registered with url " +blogUrl);
            request.Method = Method.POST;
            var response = client.Execute(request);
            return Content(response.StatusCode.ToString());
        }
    }
}
