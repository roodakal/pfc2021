﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models;
using StackExchange.Redis;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace WebApplication1.DataAccess.Repositories
{
    public class CacheRepository : ICacheRepository
    {
        private IDatabase db;
        private readonly IConfiguration _config;
        public CacheRepository(IConfiguration config)
        {
            _config = config;
            string connectionString = _config.GetConnectionString("CacheConnection");

            var cm = ConnectionMultiplexer.Connect(connectionString);
            db = cm.GetDatabase();
        }

        public List<Category>  GetCategories()
        {
            if (db.KeyExists("cat-prices"))
            {
                string categoriesSerialized = db.StringGet("cat-prices");

                var list = JsonConvert.DeserializeObject<List<Category>>(categoriesSerialized);
                return list;
            }
            else
            {
                return new List<Category>();
            }
        }

        public void UpsertCategory(Category c)
        {
            //var originalList = GetCategories();

            //var existentCategory = originalList.SingleOrDefault(x => x.Name == c.Name);

            //if (existentCategory != null)
            //{
            //    //update part

            //    existentCategory.Price = c.Price;
            //    existentCategory.Name = c.Name;
            //}
            //else
            //{
            //    //insert it

            //    originalList.Add(c);
            //}

            //var serializedCategories = JsonConvert.SerializeObject(originalList);
            //db.StringSet("cat-prices", serializedCategories);
        }

        List<Models.Category> ICacheRepository.GetCategories()
        {
            throw new NotImplementedException();
        }

        public void UpsertCategory(Models.Category c)
        {
            throw new NotImplementedException();
        }
    }
}
